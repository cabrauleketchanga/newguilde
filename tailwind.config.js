import withMT from "@material-tailwind/html/utils/withMT";

/** @type {import('tailwindcss').Config} */
module.exports = withMT({
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors: {
        primary: '#171717',
        secondary: '#222222',
        accent: '#EEA550',
        textColor: '#fff',
        secondaryTextColor: '#aaaaaa',
        borderColor: '#808080',
        gray: {
          100: '#F8F8F8',
          150: '#ebebeb',
          200: '#EAEAEA',
          300: '#D2D2D2',
          400: '#A6A6A6',
          500: '#7F7F7F',
          600: '#525252',
          700: '#393939',
          800: '#262626',
          900: '#171717',
        },

      },
      fontFamily: {
        semiBold: ['SemiBold'],
        bold: ['Bold'],
        light: ['Light'],
        medium: ['Medium'],
        rxtraBold: ['ExtraBold'],
        regular: ['Regular'],
        fontAwersome: ['fontAwersome'],
      },
      padding: {
        'tooSmall': '2px',
        'small': '5px',
        'medium': '8px',
        'normal': '10px',
        'large': '15px',
        'veryLarge': '20px',
        'big': '25px',
        'veryLarge': '30px',
        'veryBig': '40px'
      },
      margin: {
        'tooSmall': '2px',
        'small': '5px',
        'medium': '8px',
        'normal': '10px',
        'large': '15px',
        'veryLarge': '20px',
        'big': '25px',
        'veryLarge': '30px',
        'veryBig': '40px'
      },
      fontSize: {
        sm: '0.8rem',
        base: '1rem',
        xl: '1.25rem',
        '2xl': '1.563rem',
        '3xl': '1.953rem',
        '4xl': '2.441rem',
        '5xl': '3.052rem',
      }
    },
  },
  plugins: [require("daisyui")],

})
