import { ExperienceModel } from "../domain/models/experience.model";
import { JobModel } from "../domain/models/job.model";
import { ProfilModel } from "../domain/models/profil.model";

export const ProfileData: ProfilModel[] = [
    {
        id: "1",
        name: "Donald Akra",
        experienceOld: '7',
        role: ".NET Backend Developer& Blazor and MAUI Software Engineer",
        location: 'Dakar,Sénégal',
        image: "assets/avatar.png",

    },
    {
        id: "2",
        name: "Chris Ousman",
        experienceOld: '8',
        role: "Full Stack Engineer",
        location: 'Dakar,Sénégal',
        image: "assets/avatar1.png",

    },
    {
        id: "2",
        name: "Chris Ousman",
        experienceOld: '8',
        role: "Full Stack Engineer",
        location: 'Dakar,Sénégal',
        image: "assets/avatar2.png",

    },
    {
        id: "3",
        name: "Castro Lianou",
        experienceOld: '10',
        role: "Python Developer& Odoo-ERP Engineer",
        location: 'Yaoundé,Douala',
        image: "assets/avatar3.png",
    },
    {
        id: "4",
        name: "Merveille Zenap",
        experienceOld: '9',
        role: "Angular and  Java Backend(Sprint Boot) Developer",
        location: 'Yaoundé,Douala',
        image: "assets/avatar4.png",
    },
    {
        id: "5",
        name: "Merveille Zenap",
        experienceOld: '9',
        role: "Angular and  Java Backend(Sprint Boot) Developer",
        location: 'Yaoundé,Cameroun',
        image: "assets/avatar5.png",

    },
    {
        id: "6",
        name: "Kanté Soulé",
        experienceOld: '5',
        role: "Mobile Engineer(Flutter/React Native)",
        location: 'Abidjan,Cote-ivoire',
        image: "assets/avatar6.png",
    },
    {
        id: "7",
        name: "Sophiane Barga",
        experienceOld: '6',
        role: "Full stack Js developer (NodeJs/React Js& Next Js)",
        location: 'Abidjan,Cote-ivoire',
        image: "assets/avatar7.png",
    },
    {
        id: "8",
        name: "Audrey Triphène",
        experienceOld: '8',
        role: "Golang & C++ Backend Developer",
        location: 'Douala,Cameroon',
        image: "assets/avatar8.png",
    },

]
export const ExperienceData: ExperienceModel[] = [
    {
        id: '1',
        company: 'ZuumPay',
        role: 'Lead Developer',
        description: 'ZuumPay is a global cashless payment system, currency exchange, and borderless remittance network.\n - I conducted planning, analysis, design, and architected the system.\n- I developed most of the micro-services such as Authentication, Account, Wallet, Transactions, etc.\n- I manage the integration of modules done by other developers.\n- I performed general testing of the system.',
        duration: 'Feb 2023 - Currently',
    },
    {
        id: '2',
        company: 'nanodev',
        role: 'Senior Full Stack Engineerring',
        description: 'Main Stack: Mongo DB, Express, React, Node.js, Typescript, Python.\n- I contribute to in-house and clients’ projects.\n- I assist the HR department in the technical phases of our recruitment process.\n- I oversee the work of junior developers.',
        duration: 'Dec 2021 - Feb 2023',
    },
    {
        id: '3',
        company: 'insightsoftware',
        role: 'Senior Full Stack Engineer',
        description: '',
        duration: 'Feb 2023 - Dec 2023',
    },
    {
        id: '4',
        company: 'Freelance',
        role: 'Full Stack Developer',
        description: 'I solely handled the development of complete projects in freelancing.\n - Njangi\n – Mobile application to manage group savings.\n- Wangabet\n – Sport: betting website.\n- ValaGas\n – Mobile application for cooking\n - gas delivery.\n- JirethTravels – Website of a travel agency.',
        duration: 'Mar 2020 - Feb 2023',
    },
    {
        id: '5',
        company: 'Njorku Limited',
        role: 'Software Development Engineer',
        description: 'I  contributed to the development of projects such as:\n- FNE Tchad – Governmental website.\n- GoldenRemy – E- commerce.\n- Katika\n – Invoices management.\n- Noamhouse – E - commerce.\nI used Laravel most of the time but also OpenCart for E - commerce websites.',
        duration: 'Aug 2018 - Oct 2019',
    },

];

export const jobData: JobModel[] = [
    {
        id: "1",
        company: "STAR TEC INFORMATIQUE",
        role: "Devops Cloud AZURE",
        description: "SquareOne renforce ses effectifs et intègre de nouveaux ingénieurs DevOps expérimentés pour un projet client dans le domaine Défense / Aerospace.\nMissions types :\nMise en place CI/CD\nInstallation et config des outils de la chaine\nConception, déploiement, MCO des solutions\nGestion de la supervision / Sécurité / Disponibilité",
        duration: "2ans",
        datePosted: "23 heures",
        timePerWk: "40 hours/week",
        required: ["Teraform", "Github", 'Ansible', "Teragrunh"],
        industries: [],
        estimateLength: "2ans"

    },
    {
        id: "2",
        company: "ALTEN",
        role: "Ingenieur de validation",
        description: "Nous recherchons pour l'un de nos clients, du secteur des Télécommunications, une ou un Freelance en qualité d'Ingénieur Validation Hardware ( H/F ) afin de travailler sur la vérification de la bonne conformité des équipement de type Gateway . ",
        duration: "+6 mois",
        datePosted: "2 heures",
        timePerWk: "35 hours/week",
        required: ["QA", "Software", 'HO/BO', "Validation", "Gateway"],
        industries: ["Telecom", "Mutimédia"],
        estimateLength: "+6 mois"

    },
    {
        id: "3",
        company: "WEKEY",
        role: "Administrateur base de données",
        description: "",
        duration: "+6 mois",
        datePosted: "2 heures",
        timePerWk: "35 hours/week",
        required: ["SGBD Oracle", "SQL Server", 'Postgre', "EXACC", "Systemes Open Unix HPUX et AIX"],
        industries: ["Telecom", "Mutimédia"],
        estimateLength: "+6 mois"

    },
    {
        id: "4",
        company: "MC Office",
        role: "Data Reliability Engineer",
        description: "Nous recherchons un Data Reliability Engineer pour rejoindre notre équipe data support. Le candidat idéal sera passionné(e) par la finance, désireux(se) d’apprendre et de relever des défis",
        duration: "+6 mois",
        datePosted: "1 Mois",
        timePerWk: "40 hours/week",
        required: ["Linux", "Python", 'Postgress', "Excel"],
        industries: ["Finance"],
        estimateLength: "+6 mois"

    },
    {
        id: "5",
        company: "Mind quest",
        role: "Full Stack Developer JS",
        description: "Au sein de la Digital Tools Unit de mon client qui est en charge de développer et intégrer les outils IT permettant la digitalisation du parcours client.\nLa Squad Component, au sein de laquelle vous opérerez, est en charge du développement de la console d'administration de l'ensemble des produits et services.\nCette mission s’inscrit dans le cadre d'un projet de changement de stack technique",
        duration: "Temp plein",
        datePosted: "1 jour",
        timePerWk: "40 hours/week",
        required: ["Web Development", "Go", "Node.JS", "Angular", "React"],
        industries: ["ESN"],
        estimateLength: "~~"

    }

];