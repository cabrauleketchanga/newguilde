import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import { CookieService } from "ngx-cookie-service";

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    isLoggedIn = false;
    constructor(

        private cookieService: CookieService,
        private router: Router
    ) {

    }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        console.log("My url", state, route, this.router.url)
        return this.chechLoggedIn(state.url);
    }
    async chechLoggedIn(url: string): Promise<boolean> {
        let cookieValue = this.cookieService.get('token');
        console.log(cookieValue.length);
        console.log(cookieValue.length != 0);
        if (cookieValue.length != 0) {

            if (cookieValue == 'user') {
                if (url.includes('missions') || url.includes('discover_talent')) {
                    this.router.navigate(['/profile'])
                }
            } else {
                if (url.includes('profile') || url.includes('jobs')) {
                    this.router.navigate(['/missions'])
                }
            }

            return true;

        } else {
            this.router.navigate(['/login'])
            return false;
        }

    }

}