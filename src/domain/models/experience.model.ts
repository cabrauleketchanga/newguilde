export interface ExperienceModel {
    id: string;
    company: string;
    role: string;
    description: string;
    duration: string;

}
