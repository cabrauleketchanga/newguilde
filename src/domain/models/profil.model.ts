export interface ProfilModel {
    id: string;
    name: string;
    experienceOld: string;
    role: string;
    location: string;
    image: string;

}
