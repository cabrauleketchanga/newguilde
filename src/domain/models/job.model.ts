export interface JobModel {
    id: string;
    company: string;
    role: string;
    description: string;
    duration: string;
    datePosted: string;
    timePerWk: string;
    required: string[];
    industries: string[];
    estimateLength: string;

}
