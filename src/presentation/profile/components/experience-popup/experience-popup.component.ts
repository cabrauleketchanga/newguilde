import { Component } from '@angular/core';

@Component({
  selector: 'app-experience-popup',
  templateUrl: './experience-popup.component.html',
  styleUrl: './experience-popup.component.css'
})
export class ExperiencePopupComponent {
  value?: string;
  switchValue = true;

  date = null;

  onChange(result: Date[]): void {
    console.log('onChange: ', result);

  }
}
