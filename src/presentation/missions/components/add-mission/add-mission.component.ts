import { Component } from '@angular/core';

@Component({
  selector: 'app-add-mission',
  templateUrl: './add-mission.component.html',
  styleUrl: './add-mission.component.css'
})
export class AddMissionComponent {
  value?: string;
  switchValue = true;
  showPartTime: boolean = false;
  listTypeJobs = [
    { id: 1, value: "Full Time", isSelected: true },
    { id: 2, value: "Part Time", isSelected: false }
  ];
  items = ['Javascript', 'Typescript'];

  listTypeState = [
    { id: 1, value: "In production", isSelected: true },
    { id: 2, value: "Working prototype", isSelected: false },
    { id: 3, value: "Design and wireframes", isSelected: false },
    { id: 4, value: "Project is unstarted", isSelected: false }
  ];
  date = null;

  onChange(result: Date[]): void {
    console.log('onChange: ', result);

  }
  isAllSelectedJobs(item: any) {
    this.listTypeJobs.forEach((val: any) => {
      if (val.id == item.id) val.isSelected = !val.isSelected;
      else {
        val.isSelected = false;
      }
    });
    if (this.listTypeJobs[1].isSelected == true) {
      this.showPartTime = true;
    } else {
      this.showPartTime = false;
    }
  }
  isAllSelectedState(item: any) {
    this.listTypeState.forEach((val: any) => {
      if (val.id == item.id) val.isSelected = !val.isSelected;
      else {
        val.isSelected = false;
      }
    });
  }
}
