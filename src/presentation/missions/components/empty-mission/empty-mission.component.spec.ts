import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmptyMissionComponent } from './empty-mission.component';

describe('EmptyMissionComponent', () => {
  let component: EmptyMissionComponent;
  let fixture: ComponentFixture<EmptyMissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EmptyMissionComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(EmptyMissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
