import { Component } from '@angular/core';
import {
  trigger, style, animate, transition
} from '@angular/animations';
@Component({
  selector: 'app-missions',
  templateUrl: './missions.component.html',
  styleUrl: './missions.component.css',
  animations: [
    trigger('fadePage', [
      transition(':enter', [
        style({ opacity: 0, }),
        animate(500, style({ opacity: 1, }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate(500, style({ opacity: 0 }))
      ])
    ]),

  ]
})
export class MissionsComponent {

}
