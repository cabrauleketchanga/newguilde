import { Component } from '@angular/core';
import { jobData } from '../../data/dummy.data';
import {
  trigger, style, animate, transition
} from '@angular/animations';
@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrl: './jobs.component.css',
  animations: [
    trigger('fadeHeader', [
      transition(':enter', [
        style({ opacity: 0, transform: 'translateX(-250px)' }),
        animate(500, style({ opacity: 1, transform: 'translateX(0px)' }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate(500, style({ opacity: 0 }))
      ])
    ]),
    trigger('fadeJobItem', [
      transition(':enter', [
        style({ opacity: 0, transform: 'translateY(100px)' }),
        animate(500, style({ opacity: 1, transform: 'translateY(0px)' }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate(500, style({ opacity: 0 }))
      ])
    ]),
  ]
})
export class JobsComponent {
  sort: boolean = false;
  log(data: string): void {
    console.log(data);
  }

  jobs = jobData;
  handleClick() {
    const e = document.getElementById('drop');
    e?.focus();
  };
  sortShow(show: boolean) {
    this.sort = show;
  }
}
