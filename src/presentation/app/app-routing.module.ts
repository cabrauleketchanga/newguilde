import { NgModule } from "@angular/core";
import { ROUTES, RouterModule, Routes } from "@angular/router";
import { AppComponent } from "./app.component";
import { ProfileComponent } from "../profile/profile.component";
import { LayoutComponent } from "../layout/layout.component";
import { JobsComponent } from "../jobs/jobs.component";
import { MissionsComponent } from "../missions/missions.component";
import { DiscoverTalentComponent } from "../discover-talent/discover-talent.component";
import { AuthComponent } from "../auth/auth.component";
import { AuthGuard } from "../../auth/auth.guard";
import { CookieService } from "ngx-cookie-service";


const routes: Routes = [
    {
        path: 'login', component: AuthComponent,
    },
    {

        path: '', component: LayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                redirectTo: 'missions', pathMatch: 'full',
            },
            {
                path: '',
                redirectTo: 'profile', pathMatch: 'full',
            },


            {
                path: 'profile',
                component: ProfileComponent,
                canActivate: [AuthGuard],

            },
            {
                path: 'jobs',
                component: JobsComponent,
                canActivate: [AuthGuard],

            },

            {
                path: 'missions',
                component: MissionsComponent,
                canActivate: [AuthGuard],
            },
            {
                path: 'discover_talent',
                component: DiscoverTalentComponent,
            },

        ]
    },
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
