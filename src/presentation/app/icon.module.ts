import { NgModule } from "@angular/core";
import { MaterialModule } from "./material.module";
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
@NgModule({
    imports: []
})
export class IconModule {
    private path = "./assets/icons";
    constructor(private domSanitizer: DomSanitizer,
        public matIconRegistry: MatIconRegistry) {
        this.matIconRegistry.addSvgIcon("edit", this.setPath(`${this.path}/edit.svg`))
            .addSvgIcon("add", this.setPath(`${this.path}/add.svg`))
            .addSvgIcon("link", this.setPath(`${this.path}/link.svg`))
            .addSvgIcon("building", this.setPath(`${this.path}/building.svg`))
            .addSvgIcon("trash", this.setPath(`${this.path}/trash.svg`))
            .addSvgIcon("close", this.setPath(`${this.path}/close.svg`))
            .addSvgIcon("search", this.setPath(`${this.path}/search.svg`))
            .addSvgIcon("chevron_down", this.setPath(`${this.path}/chevron_down.svg`))
            .addSvgIcon("chevron_right", this.setPath(`${this.path}/chevron_right.svg`))
            .addSvgIcon("rectangle", this.setPath(`${this.path}/rectangle.svg`))
            .addSvgIcon("calendar", this.setPath(`${this.path}/calendar.svg`))
            .addSvgIcon("calendar_time", this.setPath(`${this.path}/calendar_time.svg`))
            .addSvgIcon("house", this.setPath(`${this.path}/house.svg`))
            .addSvgIcon("interval", this.setPath(`${this.path}/interval.svg`))
            .addSvgIcon("ribbon", this.setPath(`${this.path}/ribbon.svg`))
            .addSvgIcon("alert", this.setPath(`${this.path}/alert.svg`))
            .addSvgIcon("eye", this.setPath(`${this.path}/eye.svg`))
            .addSvgIcon("filters", this.setPath(`${this.path}/filters.svg`))
            .addSvgIcon("heart", this.setPath(`${this.path}/heart.svg`))
            .addSvgIcon("location", this.setPath(`${this.path}/location.svg`))

    }
    private setPath(url: string): SafeResourceUrl {
        return this.domSanitizer.bypassSecurityTrustResourceUrl(url);
    }
}