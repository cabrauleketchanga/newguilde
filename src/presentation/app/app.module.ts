import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from '../layout/layout.component';
import { IconModule } from './icon.module';
import { MatIconModule } from '@angular/material/icon';
import { ProfileComponent } from '../profile/profile.component';
import { ExperiencePopupComponent } from '../profile/components/experience-popup/experience-popup.component';
import { NzInputModule } from 'ng-zorro-antd/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NZ_I18N, en_US } from 'ng-zorro-antd/i18n';
import { JobsComponent } from '../jobs/jobs.component';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { PaginationComponent } from '../widgets/pagination/pagination.component';
import { MissionsComponent } from '../missions/missions.component';
import { DiscoverTalentComponent } from '../discover-talent/discover-talent.component';
import { EmptyMissionComponent } from '../missions/components/empty-mission/empty-mission.component';
import { AddMissionComponent } from '../missions/components/add-mission/add-mission.component';
import { AuthComponent } from '../auth/auth.component';
import { LoadingComponent } from '../widgets/loading/loading.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import {
    trigger, state, style, animate, transition, query, group
} from '@angular/animations';
@NgModule({
    declarations: [
        AppComponent,
        LayoutComponent,
        ProfileComponent,
        ExperiencePopupComponent,
        JobsComponent,
        MissionsComponent,
        DiscoverTalentComponent,
        PaginationComponent,
        EmptyMissionComponent,
        AddMissionComponent,
        LoadingComponent,
        AuthComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        BrowserAnimationsModule,
        IconModule,
        HttpClientModule,
        MatIconModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NzCheckboxModule,
        NzFormModule,
        NzInputModule,
        NzPaginationModule,



    ],

    providers: [{ provide: NZ_I18N, useValue: en_US }],
    bootstrap: [AppComponent],

})
export class AppModule { }

