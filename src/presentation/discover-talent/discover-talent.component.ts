import { Component } from '@angular/core';
import { ProfileData } from '../../data/dummy.data';
import {
  trigger, style, animate, transition
} from '@angular/animations';
@Component({
  selector: 'app-discover-talent',
  templateUrl: './discover-talent.component.html',
  styleUrl: './discover-talent.component.css',
  animations: [
    trigger('fadeFilter', [
      transition(':enter', [
        style({ opacity: 0, transform: 'translateX(-250px)' }),
        animate(500, style({ opacity: 1, transform: 'translateX(0px)' }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate(500, style({ opacity: 0 }))
      ])
    ]),
    trigger('fadeProfiles', [
      transition(':enter', [
        style({ opacity: 0, transform: 'translateY(250px)' }),
        animate(500, style({ opacity: 1, transform: 'translateY(0px)' }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate(500, style({ opacity: 0 }))
      ])
    ]),
  ]
})
export class DiscoverTalentComponent {
  listTypeJobs = [
    { id: 1, value: "Mid-Level (4-10 Years)", isSelected: false },
    { id: 2, value: "Senior- Level (+10 Years)", isSelected: false },
    { id: 3, value: "Tech-Lead/Architect", isSelected: false }
  ];
  listTypeEng = [
    { id: 1, value: "Contrat", isSelected: true },
    { id: 2, value: "Salary", isSelected: false },
  ];
  profiles = ProfileData;

  constructor() {

  }


  isAllSelectedJobs(item: any) {
    this.listTypeJobs.forEach((val: any) => {
      if (val.id == item.id) val.isSelected = !val.isSelected;
      else {
        val.isSelected = false;
      }
    });

  }
  isAllSelectedEng(item: any) {
    this.listTypeEng.forEach((val: any) => {
      if (val.id == item.id) val.isSelected = !val.isSelected;
      else {
        val.isSelected = false;
      }
    });

  }
}
