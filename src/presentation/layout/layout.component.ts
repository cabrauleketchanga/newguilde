import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrl: './layout.component.css'
})
export class LayoutComponent implements OnInit {

  isJobs = false;
  isProfile = false;
  isMissions = false;
  isDiscover = false;
  isBusy = false;
  isUser = true;

  constructor(private route: ActivatedRoute, private router: Router, private cookieService: CookieService) {

    this.initActualTab(router.url);
  }
  ngOnInit(): void {
    this.isUser = this.cookieService.get('token') === 'user';
    console.log(this.isUser);
  }


  initActualTab(url: string) {
    if (url === '/jobs') {
      this.isJobs = true;
    }
    if (url === '/profile') {
      this.isProfile = true;
    }
    if (url === '/missions') {
      this.isMissions = true;
    }
    if (url === '/discover_talent') {
      this.isDiscover = true;
    }
  }
  goToMissions() {
    this.isMissions = true;
    this.isDiscover = false;
    this.router.navigate(['missions'], { relativeTo: this.route });
  }
  goToDiscover() {
    this.isMissions = false;
    this.isDiscover = true;
    this.router.navigate(['discover_talent'], { relativeTo: this.route });
  }
  goToJobs() {
    this.isJobs = true;
    this.isProfile = false;
    this.router.navigate(['jobs'], { relativeTo: this.route });
  }
  goToProfile() {
    this.isJobs = false;
    this.isProfile = true;
    this.router.navigate(['profile'], { relativeTo: this.route });
  }
  logout(): void {
    this.isBusy = true;
    this.cookieService.delete('token')
    setTimeout(() => {
      this.isBusy = false;
      this.router.navigate(['/login'])

    }, 3000);
  }
}


