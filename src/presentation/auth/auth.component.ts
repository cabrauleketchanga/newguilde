import { Component } from '@angular/core';
import { FormControl, FormGroup, NonNullableFormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrl: './auth.component.css'
})
export class AuthComponent {
  email?: string;
  password?: string;
  isBusy: boolean = false;
  validateForm: FormGroup<{
    email: FormControl<string>;
    password: FormControl<string>;
    remember: FormControl<boolean>;
  }> = this.fb.group({
    email: ['', [Validators.required]],
    password: ['', [Validators.required]],
    remember: [true]
  });

  submitForm(): void {

    if (this.validateForm.valid) {
      console.log('submit', this.validateForm.value);
      if (this.validateForm.value.email?.includes('guilde')) {
        this.cookieService.set('token', 'recruteur')
      } else {
        this.cookieService.set('token', 'user')

      }
      this.isBusy = true;
      setTimeout(() => {
        this.isBusy = false;
        this.router.navigate([''])

      }, 3000);

    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  constructor(private fb: NonNullableFormBuilder, private router: Router, private cookieService: CookieService,) { }

}
